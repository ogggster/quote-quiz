﻿using QuoteQuiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuoteQuiz.Services.Contracts
{
    public interface IAnswerService
    {
        Answer GetById(int id);

        IQueryable<Answer> All();

        Answer GetRandom();
    }
}
