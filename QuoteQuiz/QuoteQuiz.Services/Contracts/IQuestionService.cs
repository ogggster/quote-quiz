﻿using QuoteQuiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuoteQuiz.Services.Contracts
{
    public interface IQuestionService
    {
        Question GetById(int id);

        IQueryable<Question> All();
    }
}
