﻿using QuoteQuiz.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuoteQuiz.Models;
using QuoteQuiz.Data.Repositories;

namespace QuoteQuiz.Services
{
    public class QuestionService : IQuestionService
    {
        IRepository<Question> questions;

        public QuestionService(IRepository<Question> questions)
        {
            this.questions = questions;
        }

        public IQueryable<Question> All()
        {
            return this.questions.All();
        }

        public Question GetById(int id)
        {
            return this.questions.GetById(id);
        }
    }
}
