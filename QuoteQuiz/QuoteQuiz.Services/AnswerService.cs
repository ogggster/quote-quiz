﻿using QuoteQuiz.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuoteQuiz.Models;
using QuoteQuiz.Data.Repositories;

namespace QuoteQuiz.Services
{
    public class AnswerService : IAnswerService
    {
        IRepository<Answer> answers;

        public AnswerService(IRepository<Answer> answers)
        {
            this.answers = answers;
        }

        public IQueryable<Answer> All()
        {
            return this.answers.All();
        }

        public Answer GetById(int id)
        {
            return this.answers.GetById(id);
        }

        public Answer GetRandom()
        {
            return this.answers.GetRandom();
        }
    }
}
