﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuoteQuiz.Models
{
    public class Question
    {
        private ICollection<Answer> answers;

        public Question()
        {
            this.answers = new HashSet<Answer>();
        }

        public int Id { get; set; }

        [StringLength(300)]
        public string Description { get; set; }
        
        public int CorrectAnswerId { get; set; }

        [ForeignKey("CorrectAnswerId")]
        public virtual Answer Answer { get; set; }

        public virtual ICollection<Answer> Answers { get { return this.answers; } set { this.answers = value; } }
    }
}
