﻿using QuoteQuiz.Models;
using System.Data.Entity;
using Constants;

namespace QuoteQuiz.Data
{
    public class QuoteQuizDbContext : DbContext, IQuoteQuizDbContext
    {
        public QuoteQuizDbContext()
            : base(App.ConnectionString)
        {
        }

        public virtual IDbSet<Question> Question { get; set; }

        public virtual IDbSet<Answer> Answer { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Question>()
                .HasRequired(c => c.Answer)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}