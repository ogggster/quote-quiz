namespace QuoteQuiz.Data.Migrations
{
    using QuoteQuiz.Models;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<QuoteQuizDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(QuoteQuizDbContext context)
        {
            if (!context.Question.Any())
            {
                QuestionsAndAnswers(context);
            }
        }

        private void QuestionsAndAnswers(QuoteQuizDbContext context)
        {
            var answers = new List<Answer>()
            {
                new Answer { Id = 1, Text = "H. G. Wells" },
                new Answer { Id = 2, Text = "Napoleon Bonaparte" },
                new Answer { Id = 3, Text = "Bertrand Russell" }
            };

            context.Question.Add(new Question
            {
                Description = "Moral indignation is jealousy with a halo.",
                CorrectAnswerId = 1,
                Answers = new List<Answer>(answers)
            });
            context.Question.Add(new Question
            {
                Description = "Glory is fleeting, but obscurity is forever.",
                CorrectAnswerId = 2,
                Answers = new List<Answer>(answers)
            });
            context.Question.Add(new Question
            {
                Description = "The whole problem with the world is that fools and fanatics are always so certain of themselves, and wiser people so full of doubts.",
                CorrectAnswerId = 3,
                Answers = new List<Answer>(answers)
            });

            context.SaveChanges();
        }
    }
}