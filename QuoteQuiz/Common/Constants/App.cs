﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constants
{
    public static class App
    {
        public const string ConnectionString = "QuoteQuizConnection";
        public const string CurrentQuestionCacheKey = "current-question";
        public const string CurrentAnswerCacheKey = "current-answer";
        public const string QuestionCountCacheKey = "questions-count";
        public const string CorrectAnswer = "Correct!The right answer is: ";
        public const string WrongAnswer = "Sorry, you are wrong! The right answer is: ";
        public const string ButtonYesLabel = "Yes";
        public const string ButtonNoLabel = "No";
    }
}
