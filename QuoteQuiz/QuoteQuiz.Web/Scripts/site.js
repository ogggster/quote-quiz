﻿$(function () {
    'use strict'
    const baseUrl = 'http://localhost:59533/';
    localStorage.setItem('question-mode', 'binary');

    function sendDataToServer(url, method, data, callback) {
        $.ajax({
            url: baseUrl + url,
            data: data,
            method: method
        })
            .done((data) => callback(null, data))
            .fail((error) => callback(error, data));
    }

    function settingsQuestionUpdate(error, data) {
        if (error) {
            throw new Error(error);
        }
        $('#question-area').html(data);
        if ($('.next-container')) {
            $('.next-container').hide();
            $('#next-question').addClass('hidden');//.hide();
        }
        if (localStorage.getItem('question-mode') == 'binary') {
            $('.binary-button').on('click', (e) => {
                sendDataToServer('Quote/Guess', 'GET', { 'userSuggested': e.target.id }, answeredQuestionResponse);
            });
        } else {
            $('.multiple-button').on('click', (e) => {
                sendDataToServer('Quote/Guess', 'GET', { 'userSuggested': e.target.id }, answeredQuestionResponse);
            });
        }
    }

    $('input').on('click', (e) => {
        if (e.currentTarget.id != localStorage.getItem('question-mode')) {
            localStorage.setItem('question-mode', e.currentTarget.id);
            sendDataToServer('Quote/GetQuestion', 'GET', { 'type': e.currentTarget.id }, settingsQuestionUpdate);
        }
    });

    function answeredQuestionResponse(error, data) {
        if (error) {
            throw new Error(error);
        }
        $('#response-area').html(data);
        $('.result-container').fadeOut(2000);
        $('#answers-container').fadeOut(2000, () => {
            $('.next-container').show();
            $('#next-question').removeClass('hidden');
        });
    }

    $('.binary-button').on('click', (e) => {
        sendDataToServer('Quote/Guess', 'GET', { 'userSuggested': e.target.id }, answeredQuestionResponse);
    });

    $('#next-question').on('click', (e) => {
        $('#next-question').addClass('hidden');
        $('.next-container').hide();
        sendDataToServer('Quote/GetQuestion', 'GET', { 'type': localStorage.getItem('question-mode') }, settingsQuestionUpdate);
    });
});