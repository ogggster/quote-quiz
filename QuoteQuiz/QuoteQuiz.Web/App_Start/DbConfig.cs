﻿using QuoteQuiz.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace QuoteQuiz.Web
{
    public class DbConfig
    {
        public static void Initialize()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<QuoteQuizDbContext, Data.Migrations.Configuration>());
        }
    }
}