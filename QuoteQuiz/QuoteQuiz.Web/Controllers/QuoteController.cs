﻿using Constants;
using QuoteQuiz.Models;
using QuoteQuiz.Services.Contracts;
using QuoteQuiz.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuoteQuiz.Web.Controllers
{
    public class QuoteController : Controller
    {
        protected readonly IQuestionService questions;
        protected readonly IAnswerService answers;

        public QuoteController(IQuestionService questions, IAnswerService answers)
        {
            this.questions = questions;
            this.answers = answers;
        }

        public ActionResult Index()
        {
            Question question = this.questions.GetById(1);
            int numberOfQuestions = this.questions.All().Count();
            HttpRuntime.Cache.Insert(App.CurrentQuestionCacheKey, question);
            HttpRuntime.Cache.Insert(App.QuestionCountCacheKey, numberOfQuestions);
            var questionToView = ToBinaryQuestion(question);

            return View(questionToView);
        }

        [HttpGet]
        public ActionResult Guess(string userSuggested)
        {
            bool answerGuessed;
            var question = (Question)HttpRuntime.Cache.Get(App.CurrentQuestionCacheKey);
            var answer = (Answer)HttpRuntime.Cache.Get(App.CurrentAnswerCacheKey);

            if (Request.IsAjaxRequest())
            {
                answerGuessed = IsAnswerCorrect(userSuggested, question, answer);
            }
            else
            {
                return this.Content(string.Empty);
            }

            var author = question.Answers.Where(a => a.Id == question.CorrectAnswerId).FirstOrDefault();
            QuestionResult result = new QuestionResult() { Author = author.Text };

            result.General = answerGuessed ?
                App.CorrectAnswer : 
                App.WrongAnswer;

            return PartialView("_result", result);
        }

        [HttpGet]
        public ActionResult GetQuestion(string type)
        {
            Question previousQuestion = (Question)HttpRuntime.Cache.Get(App.CurrentQuestionCacheKey);
            int allQuestions = (int)HttpRuntime.Cache.Get(App.QuestionCountCacheKey);
            int indexToGet = previousQuestion.Id == allQuestions ? 
                1 : 
                previousQuestion.Id + 1;

            Question newQuestion = this.questions.GetById(indexToGet);

            HttpRuntime.Cache.Insert(App.CurrentQuestionCacheKey, newQuestion);

            if (type == "binary")
            {
                var questionToBinaryPartial = this.ToBinaryQuestion(newQuestion);
                return PartialView("_binary_partial", questionToBinaryPartial);
            }
            else
            {
                var questionToMultiplePartial = this.ToMultipleQuestion(newQuestion);
                return PartialView("_multiple_partial", questionToMultiplePartial);
            }
        }

        private bool IsAnswerCorrect(string userSuggested, Question question, Answer answer)
        {
            int number;
            bool parsed = Int32.TryParse(userSuggested, out number);
            bool guessed = parsed ? 
                GuessMultipleQuestion(userSuggested, question) : 
                GuessBinaryQuestion(userSuggested, question, answer);

            return guessed;
        }

        private BinaryQuestionViewModel ToBinaryQuestion(Question question)
        {
            Random r = new Random();
            int random = r.Next(0, question.Answers.Count);

            Answer answer = question.Answers.ToList()[random];
            HttpRuntime.Cache.Insert(App.CurrentAnswerCacheKey, answer);
            return new BinaryQuestionViewModel { Description = question.Description, AuthorToGuess = answer.Text };
        }

        private MultipleQuestionViewModel ToMultipleQuestion(Question question)
        {
            return new MultipleQuestionViewModel { Description = question.Description, Answers = question.Answers };
        }

        private bool GuessBinaryQuestion(string result, Question question, Answer answer)
        {
            return ((question.CorrectAnswerId == answer.Id && result == App.ButtonYesLabel)
                || (question.CorrectAnswerId != answer.Id && result == App.ButtonNoLabel));
        }

        private bool GuessMultipleQuestion(string result, Question question)
        {
            return question.CorrectAnswerId == int.Parse(result);
        }
    }
}