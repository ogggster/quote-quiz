﻿using QuoteQuiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuoteQuiz.Web.Models
{
    public class MultipleQuestionViewModel : QuestionViewModel
    {
        public IEnumerable<Answer> Answers { get; set; }
    }
}