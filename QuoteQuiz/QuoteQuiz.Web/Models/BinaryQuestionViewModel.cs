﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuoteQuiz.Web.Models
{
    public class BinaryQuestionViewModel : QuestionViewModel
    {
        private readonly string[] answers;
        
        public BinaryQuestionViewModel()
        {
            this.answers = new string[] { "Yes", "No" };
        }

        public string AuthorToGuess { get; set; }

        public IEnumerable<string> Answers { get { return this.answers;  } } 
    }
}