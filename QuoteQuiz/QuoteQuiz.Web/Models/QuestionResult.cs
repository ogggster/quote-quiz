﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuoteQuiz.Web.Models
{
    public class QuestionResult
    {
        public string General { get; set; }

        public string Author { get; set; }
    }
}