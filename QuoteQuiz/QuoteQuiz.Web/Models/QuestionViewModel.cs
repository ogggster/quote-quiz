﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuoteQuiz.Web.Models
{
    public class QuestionViewModel
    {
        public string Description { get; set; }
    }
}